# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=thunderbird
pkgver=52.9.0
pkgrel=1
pkgdesc="Email client from Mozilla"
url="https://www.mozilla.org/thunderbird/"
arch="all"
options="!check"  # X11 required
license="MPL"
depends=""
# moz build system stuff
# python deps
# system-libs
# actual deps
makedepends="
	autoconf2.13 ncurses-dev perl cmd:which

	ncurses-dev openssl-dev

	alsa-lib-dev bzip2-dev icu-dev libevent-dev libffi-dev libpng-dev
	libjpeg-turbo-dev nspr-dev nss-dev pulseaudio-dev zlib-dev

	dbus-glib-dev gconf-dev gtk+2.0-dev gtk+3.0-dev hunspell-dev libsm-dev
	libnotify-dev libxcomposite-dev libxdamage-dev libxrender-dev libxt-dev
	nss-static sqlite-dev startup-notification-dev unzip yasm zip
	"
install=""
subpackages="$pkgname-dev"
source="https://archive.mozilla.org/pub/thunderbird/releases/$pkgver/source/thunderbird-$pkgver.source.tar.xz
	mozconfig
	bad-google-code.patch
	fix-seccomp-bpf.patch
	mach-linux-musl.patch
	profiler.patch
	proper-system-hunspell.patch
	stab.h
	stackwalk-x86-ppc.patch
	thunderbird.desktop
	"
somask="liblgpllibs.so
	libmozgtk.so
	libmozsandbox.so
	libxul.so"
_tbirddir=/usr/lib/${pkgname}-${pkgver}

unpack() {
	default_unpack
	# just ripped from Firefox's APKBUILD...
	[ -z $SKIP_PYTHON ] || return 0
	msg "Killing all remaining hope for humanity and building Python 2..."
	cd "$srcdir"
	[ -d python ] && rm -r python
	mkdir python
	cd python
	# 19:39 <+solar> just make the firefox build process build its own py2 copy
	curl -O https://www.python.org/ftp/python/2.7.15/Python-2.7.15.tar.xz
	tar xJf Python-2.7.15.tar.xz
	cd Python-2.7.15
	# 20:03 <calvin> TheWilfox: there's always violence
	./configure --prefix="$srcdir/python"
	make -j $JOBS
	# 6 tests failed:
	#    test__locale test_os test_posix test_re test_strptime test_time
	# make test
	make -j $JOBS install
}

prepare() {
	default_prepare
	cp "$srcdir"/stab.h "$builddir"/mozilla/toolkit/crashreporter/google-breakpad/src/
	cp "$srcdir"/mozconfig "$builddir"/mozconfig
	echo "ac_add_options --enable-optimize=\"$CFLAGS\"" >> "$builddir"/mozconfig
	echo "ac_add_options --host=\"$CHOST\"" >> "$builddir"/mozconfig
	echo "ac_add_options --target=\"$CTARGET\"" >> "$builddir"/mozconfig
	# too much memory
	if [ -z "$JOBS" -o $JOBS -gt 32 ]; then
		echo "mk_add_options MOZ_MAKE_FLAGS=\"-j32\"" >> "$builddir"/mozconfig
	fi
}

build() {
	cd "$builddir"

	# reportedly needed for gcc6; confirm this?
	export CXXFLAGS="$CXXFLAGS -fno-delete-null-pointer-checks -fno-schedule-insns2"

	export LDFLAGS="$LDFLAGS -Wl,-rpath,${_tbirddir}"
	export USE_SHORT_LIBNAME=1

	export PATH="$srcdir/python/bin:$PATH"
	./mozilla/mach build
}

package() {
	cd "$builddir"
	export PATH="$srcdir/python/bin:$PATH"
	DESTDIR="$pkgdir" ./mozilla/mach install
	install -D -m644 "$srcdir"/thunderbird.desktop \
		"$pkgdir"/usr/share/applications/thunderbird.desktop
}

dev() {
	pkgdesc="$pkgdesc (development files)"

	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/include "$subpkgdir"/usr/include
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/thunderbird-devel* "$subpkgdir"/usr/lib
	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/idl "$subpkgdir"/usr/share
}

sha512sums="2142ba7cc04f48a9ffa17ad8f3a0d761f90416c9e6a7066be662a09b19846f13f0fb9669356ccbbf576744a83143cd659c28ce034679c4d6377004f428932dc1  thunderbird-52.9.0.source.tar.xz
fa45db90165fd4eff9318de4c1f6af1d48f48ceb3506cad8ecbf4bed7a22241f930db2a4083444aeb0ebad127344a0f8fbe773f16b2ea5cddedca8937370176c  mozconfig
9b11ba43f1f3fe9cda69b6b92e2073ea5165a47e30084537f396ceb8fb63573c4eb057251644837504aa4546183dc8f77fbb24f1450b6a15a1386f29180deefc  bad-google-code.patch
2f52fcd7c42f8e12c955e05aa12449aa486c5347d2a7406ff0dada66f64079152b18c3f65c43410df372e871488f17889bc337ced37d0b76305afdbcb55cb580  fix-seccomp-bpf.patch
475bdf81c41775634b131635197fa449b5068f2624a6b120d1878e2191a8e7badf01ac79a15ccf39242c64a29357f2ed7bae96352ceb70a234b17468a999e0c4  mach-linux-musl.patch
7e72b96196f51cc02478f1802a10b1c1754db09d7d35aef697c5dcaace107e7a45a1b97256cc98b4aa728845694be093b148b61868e8ebfc8317fea19d6c71fa  profiler.patch
63b09028262a109e3a02f928c12323793df65dbd6d5605ddc315978b50ff4b50f6d1af410dc7c00538c80009a8721900c6320b166c8aa9bc6dce170ebcd6fc91  proper-system-hunspell.patch
0b3f1e4b9fdc868e4738b5c81fd6c6128ce8885b260affcb9a65ff9d164d7232626ce1291aaea70132b3e3124f5e13fef4d39326b8e7173e362a823722a85127  stab.h
d620a1efa4b079ce082a27cfacbae275aceb3d268fd44bfd3f4b742b8098c8e1b1733edd360404c5109137c508b8426ed9e1ca1036b11752de8d1f429bf14844  stackwalk-x86-ppc.patch
95a2b1deb4f6c90750fdd2bfe8ca0a7879a5b267965091705a6beb0a0a4b1ccad75d11df7b9885543ca4232ff704e975c6946f4c11804cb71c471e06f9576001  thunderbird.desktop"
