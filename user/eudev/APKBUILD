# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: 
pkgname=eudev
pkgver=3.2.5
pkgrel=2
pkgdesc="OpenRC compatible fork of systemd-udev"
url="https://wiki.gentoo.org/wiki/Project:Eudev"
arch="all"
options="!checkroot"
license="GPL-2.0-only"
depends=""
depends_dev=""
replaces="udev udev-init-scripts"
provides="udev=176"
makedepends="gperf glib-dev linux-headers kmod-dev gobject-introspection-dev
	util-linux-dev"
checkdepends="tree xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs $pkgname-openrc
	$pkgname-netifnames::noarch"
source="http://dev.gentoo.org/~blueness/$pkgname/$pkgname-$pkgver.tar.gz
	default-rules.patch
	load-fbcon.patch
	udev-postmount.initd
	setup-udev
	udev.initd
	udev-settle.initd
	udev-trigger.initd
	udev.confd
	udev-settle.confd
	udev-trigger.confd
	"
builddir="$srcdir/$pkgname-$pkgver"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--sysconfdir=/etc \
		--with-rootprefix= \
		--with-rootrundir=/run \
		--with-rootlibexecdir=/lib/udev \
		--libdir=/usr/lib \
		--enable-split-usr \
		--enable-manpages \
		--disable-hwdb \
		--enable-kmod \
		--exec-prefix=/
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"

	make DESTDIR="$pkgdir" sharepkgconfigdir=/usr/lib/pkgconfig \
		install

	local i
	for i in udev udev-settle udev-trigger; do
		install -Dm755 "$srcdir"/$i.initd \
			"$pkgdir"/etc/init.d/$i
		install -Dm644 "$srcdir"/$i.confd \
			"$pkgdir"/etc/conf.d/$i
	done

	install -Dm755 "$srcdir"/udev-postmount.initd \
		"$pkgdir"/etc/init.d/udev-postmount
	install -Dm755 "$srcdir"/setup-udev \
		"$pkgdir"/sbin/setup-udev
}

dev() {
	replaces="udev-dev"
	default_dev
}

libs() {
	pkgdesc="Dynamic library to access udev device information"
	replaces="libudev"
	depends=""

	mkdir -p "$subpkgdir"/lib
	local i; for i in "$pkgdir"/usr/lib/libudev.so.*; do
		mv $i "$subpkgdir"/lib
		ln -s ../../lib/${i##*/} "$pkgdir"/usr/lib/${i##*/}
	done
}

netifnames() {
	pkgdesc="udev rules for systemd-style interface names"

	mkdir -p "$subpkgdir"/lib/udev/rules.d
	mv "$pkgdir"/lib/udev/rules.d/80-net-name-slot.rules \
		"$subpkgdir"/lib/udev/rules.d/
}

openrc() {
	replaces="udev-init-scripts-openrc"
	default_openrc
}

sha512sums="f73efde0d2dafcf79be4ebe9f6e6abb44329ea4ae45ccc4d9662c1d5ca6f4d45c27ca0b3135d7fa85f305bf7c5825dc1000079eb93fe7179c36a229fe63e372c  eudev-3.2.5.tar.gz
683e3c26ca4f058303f8db7d4977e436d728386ee6866719c4f11ff31db27572d3f486f51d3173310e1ec2cf995fa4c396f2415bdf45dabdd80e6e55e798a314  default-rules.patch
ff5928fd555e095d9f3234dd004ac4c5925405d308777e9b018e8e03112cd109935b51d75a3bc4a2a1018eb486e8a5e5ef6ee978860002a8fff93b116e9721f5  load-fbcon.patch
8ef1b911843ab13acb1c1b9b7a0a5cd76659f395c3db9e579429556f23eacebb414507dc0231e2455e7589bc70054fa1e6b6dd93dd833f7101c0da0597aabf88  udev-postmount.initd
d79c44e2879f00a0f87cdfb4971936ec201706690014b2a11634deb564cba0d53aba37b97b5595e6bc2f4bd258be33c52aba6236dc2f1a79fbb37027fde60a3d  setup-udev
5f171985505630bc7147c6c4180ce009b772b2b00ffc876693a839b02479e820dbd44644653e2adf16b8fecd8ea94da9cfb07cd239343211b2ddafa64568aacf  udev.initd
24fb7fcc37f73f3275141178e436917fc988dd7bfd97321b7603824ffbf84c5bed6573de2b9447f68c75ce704af96b45cafca27e07acd8754a6f6bc597684445  udev-settle.initd
a4dd32e702ea522095e42794568a624556ff07187161805f77f569d1d1df0eb08ea25b943afceff0c2f8d71c93aece0bbc9fe2e69f525b2af249713609e2e715  udev-trigger.initd
731eeb358ff0b25d524c6108087d0a61368e3aeabbda07f49d67e44b70ac4183cc1f4b14608351bd4572b5a9c215e7235340e634cdcfce8072bc330da3498e07  udev.confd
fa9d1e9b071b0038d9869f8be92322baea86ff6f26d4bdf254622aae156bb19afb3135f06321cd2350f0e7501d39d1f70dc1760f77b78858787ef24d4664c16b  udev-settle.confd
6f953f9a3dbe0f9ff8ce1950411a8cc77453a3d1cd4c98520ba670b1aba520e666b1d64b7e9db5480e569673d9f9440e619f8af6b81e6ce140c0a363422c2a95  udev-trigger.confd"
