# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kfloppy
pkgver=18.04.3
pkgrel=0
pkgdesc="Utility for formatting floppy diskettes"
url="https://utils.kde.org/projects/kfloppy/"
arch="all"
license="GPL-2.0"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev ki18n-dev kcoreaddons-dev
	kcompletion-dev kdoctools-dev kxmlgui-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kfloppy-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="4bdc3541bdf3e817840e81b11ab1878d71e96bc7eff2e3fe53d2877b0956588e0ec6cafc41b2661064a676e4e08c0de4c744d97146fee7ae0d3379f6850eb674  kfloppy-18.04.3.tar.xz"
