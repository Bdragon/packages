# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=marble
pkgver=18.04.3
pkgrel=0
pkgdesc="Free, open-source map and virtual globe"
url="https://marble.kde.org/"
arch="all"
options="!check"  # Test suite requires package to be already installed.
license="LGPL-2.1-only AND GPL-2.0-only"
depends="shared-mime-info"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtsvg-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev zlib-dev
	qt5-qtwebkit-dev krunner-dev kcoreaddons-dev kwallet-dev knewstuff-dev
	kio-dev kparts-dev kcrash-dev ki18n-dev phonon-dev plasma-framework-dev
	qt5-qtpositioning-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-libs"
source="https://download.kde.org/stable/applications/$pkgver/src/marble-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="26a29e6648516e99c6ae33a6253fa4922271a412eba14212af24cf33dcab8845533eb4c1a8fea11b61f20c0a5c34007ed9e0db8edb3590b01a01404ac4493704  marble-18.04.3.tar.xz"
