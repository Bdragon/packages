# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ksudoku
pkgver=18.04.3
pkgrel=0
pkgdesc="Desktop Sudoku (symbol placement / logic) game"
url="https://games.kde.org/game.php?game=ksudoku"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtsvg-dev karchive-dev kconfig-dev kconfigwidgets-dev kcrash-dev
	kcoreaddons-dev kdoctools-dev kguiaddons-dev ki18n-dev kiconthemes-dev
	kio-dev kjobwidgets-dev kwidgetsaddons-dev kxmlgui-dev libkdegames-dev
	glu-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/ksudoku-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="b205523e365c87badaa9c0b47f3228df2c678e89346329215c2a11cc652f92399d0a31edb09db992164e20addf44ba18c31b989b1d5b66b090138c656d4b32c4  ksudoku-18.04.3.tar.xz"
