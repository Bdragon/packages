# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gwenview
pkgver=18.04.3
pkgrel=0
pkgdesc="Fast and easy image viewer by KDE"
url="https://www.kde.org/applications/graphics/gwenview/"
arch="all"
options="!check"  # All tests require X11.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev phonon-dev
	kio-dev kactivities-dev kitemmodels-dev ki18n-dev kdoctools-dev
	kparts-dev kwindowsystem-dev kiconthemes-dev knotifications-dev
	libkipi-dev libjpeg-turbo-dev libpng-dev lcms2-dev zlib-dev exiv2-dev
	baloo-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/gwenview-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="a1c36a05e4ae3ee9aff74f4a56eb10c2c27aa89e10886d14dd6753e42329653550e8716965156ec5f50915e320224757bf52d1178986ef10d039e3c622c01a7f  gwenview-18.04.3.tar.xz"
