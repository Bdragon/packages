# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=khtml
pkgver=5.48.0
pkgrel=0
pkgdesc="The KDE HTML library, ancestor of WebKit"
url="https://konqueror.org/"
arch="all"
options="!check"  # Tests require X11.
license="LGPL-2.1+ AND LGPL-2.1-only"
depends=""
depends_dev="qt5-qtbase-dev kcodecs-dev ki18n-dev kparts-dev ktextwidgets-dev
	kjs-dev"
makedepends="$depends_dev cmake extra-cmake-modules giflib-dev gperf kio-dev
	karchive-dev kglobalaccel-dev kiconthemes-dev knotifications-dev
	kwallet-dev libjpeg-turbo-dev libpng-dev libx11-dev openssl-dev
	phonon-dev zlib-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/khtml-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="66d714d33550539893f4ddad26749251705579c806207e93a029cbe00a561acaab13cb2f3a6e0d9bcf99411300e8cc773f09216e078cbe4f094757fec9be0324  khtml-5.48.0.tar.xz"
