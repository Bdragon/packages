# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=klettres
pkgver=18.04.3
pkgrel=0
pkgdesc="Learn alphabets for multiple languages"
url="https://www.kde.org/applications/education/klettres/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev phonon-dev
	kcompletion-dev kcrash-dev kdoctools-dev kemoticons-dev ki18n-dev
	knewstuff-dev kwidgetsaddons-dev kconfigwidgets-dev kcoreaddons-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/klettres-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="c579a1421694a1681d68716695210ad88bf0510647a4d0957ffdd4f86ce137ffb0693bb7a0fa7ef5d0a15845ce8ed85a909fb1711597b6dc8302ae43e0435b0a  klettres-18.04.3.tar.xz"
