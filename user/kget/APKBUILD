# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kget
pkgver=18.04.3
pkgrel=0
pkgdesc="Versatile download manager"
url="https://www.kde.org/applications/internet/kget/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kcompletion-dev ki18n-dev
	kconfig-dev kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev kio-dev
	kdoctools-dev kiconthemes-dev kitemviews-dev kcmutils-dev kparts-dev
	kdelibs4support-dev knotifications-dev knotifyconfig-dev kservice-dev
	solid-dev ktextwidgets-dev kwallet-dev kwidgetsaddons-dev kxmlgui-dev
	kwindowsystem-dev gpgme-dev qca-dev boost-dev libktorrent-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kget-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="cefc16a3ecd919a6c288e3adf6b63fff55b8dcbb1517b70ebafebea1045044b28ca490bef91a50dcdcd96091523a0a0b79bb5f3c926f719138de29bb29f50489  kget-18.04.3.tar.xz"
