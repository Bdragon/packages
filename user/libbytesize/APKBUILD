# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libbytesize
pkgver=1.3
pkgrel=0
pkgdesc="Library for working with sizes in bytes"
url="https://github.com/storaged-project/libbytesize/"
arch="all"
options="!check"  # Requires locale(1).
license="LGPL-2.1+"
makedepends="gmp-dev mpfr-dev pcre-dev python3"
subpackages="$pkgname-dev $pkgname-lang py3-bytesize:py:noarch"
source="https://github.com/storaged-project/$pkgname/releases/download/$pkgver/$pkgname-$pkgver.tar.gz
	no-msgcat.patch"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--without-python2
	# XXX We do not have a msgfilter app in gettext-tiny.
	# This would be required to ship a Serbian translation.
	rm po/sr*
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

py() {
	pkgdesc="$pkgdesc (Python bindings)"
	depends="py3-six"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/python* "$subpkgdir"/usr/lib/
}

sha512sums="a8e5b5d59d0a97ae52a2c0f437c4d7fd8afc8249762be99027e3beeadc6c54ad52a09c1d30e66466b99496fcf3419fa799d0fcccf61c0661b3e9cff48d0054af  libbytesize-1.3.tar.gz
5f8b46c257553672b7c2501bae99ff44594b91bfcf3f1ee209a390a8cdda693616e8207a99cea2e1263093324807a307dac9e643d1482e14f9ba604f51a05d6d  no-msgcat.patch"
