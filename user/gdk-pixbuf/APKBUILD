# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gdk-pixbuf
pkgver=2.36.12
pkgrel=0
pkgdesc="GTK+ image loading library"
url="https://www.gtk.org/"
arch="all"
options="!check"  # bug753605-atsize.jpg is missing from tarball.
license="LGPL-2.0+"
depends="shared-mime-info"
depends_dev=""
makedepends="$depends_dev tiff-dev libjpeg-turbo-dev gobject-introspection-dev
	libpng-dev glib-dev"
install="$pkgname.pre-deinstall"
triggers="$pkgname.trigger=/usr/lib/gdk-pixbuf-2.0/*/loaders"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.gnome.org/sources/gdk-pixbuf/${pkgver%.*}/gdk-pixbuf-$pkgver.tar.xz
	"
replaces="gtk+"

# secfixes:
#   2.36.6-r1:
#     - CVE-2017-6311
#     - CVE-2017-6312
#     - CVE-2017-6314

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--with-x11 \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--with-libjpeg \
		--with-libtiff \
		--enable-introspection
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

doc() {
	replaces="gtk+-doc"
	default_doc
}

dev() {
	replaces="gtk+-dev"
	default_dev
}

sha512sums="5d3bd620cf464bf92079b15c78f8400db48c427053c6eeedc4ea9652a4c290a09a26310100cc7eb487daf4b565df9f7e3a6edf14685f81ddbff6a0652fb6b41b  gdk-pixbuf-2.36.12.tar.xz"
