# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdelibs4support
pkgver=5.48.0
pkgrel=0
pkgdesc="Legacy support for KDE 4 software"
url="https://www.kde.org/"
arch="all"
options="!check suid"  # Test requires running X11 session.
license="LGPL-2.1+ AND MIT AND LGPL-2.1-only AND LGPL-2.0-only AND (LGPL-2.1-only OR LGPL-3.0-only) AND (LGPL-2.0-only OR LGPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtsvg-dev qt5-qttools-dev kcompletion-dev
	kconfigwidgets-dev kcrash-dev kdesignerplugin kdesignerplugin-dev
	kemoticons-dev kparts-dev kunitconversion-dev kinit kded kded-dev
	kitemmodels-dev knotifications-dev sonnet-dev threadweaver-dev
	kinit-dev"
makedepends="$depends_dev cmake extra-cmake-modules"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kdelibs4support-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="76903eb30429cab5130859ac8bfa6e4b79ea0c4b7ada8b990e63c3cb5e35051213752a69df98b17283f2e713eabee469294175aae596d777df3dd3e3d9d3e306  kdelibs4support-5.48.0.tar.xz"
