# Contributor: Kiyoshi Aman <kiyoshi.aman@gmail.com>
# Maintainer: 
pkgname=libwebp
pkgver=1.0.0
pkgrel=0
pkgdesc="Libraries for working with WebP images"
url="https://developers.google.com/speed/webp/"
arch="all"
license="BSD"
makedepends="libpng-dev libjpeg-turbo-dev tiff-dev giflib-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools"
source="https://downloads.webmproject.org/releases/webp/libwebp-$pkgver.tar.gz"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-static \
		--enable-libwebpmux \
		--enable-libwebpdemux \
		--enable-libwebpdecoder
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	mkdir -p "$pkgdir"/usr/share/doc/$pkgname
	install -Dm644 PATENTS README "$pkgdir"/usr/share/doc/$pkgname
}

tools() {
	pkgdesc="The WebP command line tools"
	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="2af7036957722a3f1533fa2da0da15c76d7eb8ac98ec4ad5cf71dd4262f3d7c9897fb6b50befab83b7de22f0abceeb2c0ff52d60927513d40f8a41aa6a9abd99  libwebp-1.0.0.tar.gz"
