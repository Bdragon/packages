# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kwave
pkgver=18.04.3
pkgrel=0
pkgdesc="Sound editor built for KDE"
url="https://kwave.sourceforge.net/"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtmultimedia-dev
	karchive-dev kcompletion-dev kconfig-dev kconfigwidgets-dev kcrash-dev
	kcoreaddons-dev kdbusaddons-dev kdoctools-dev ki18n-dev kiconthemes-dev
	kio-dev kservice-dev ktextwidgets-dev kwidgetsaddons-dev kxmlgui-dev
	audiofile-dev libsamplerate-dev alsa-lib-dev pulseaudio-dev flac-dev
	id3lib-dev libmad-dev opus-dev libvorbis-dev fftw-dev librsvg-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kwave-$pkgver.tar.xz
	remove-msgcat.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="30fdfdf681bdf2f339a0b8ca91899e4b3e447075a7454f352f897745af2b671a5412c37ff372563a5f80cb3545295a5c4849b3243386ab86831e325867f7ef23  kwave-18.04.3.tar.xz
43474f73281a7e3e97e2aa9e8c5b7aac50c8153c4ec09345a9ff43eb3c90a17c1dd9fbd2c630967ff87a5b21139f4efd0ecc44f36052549cc2036fd1db1dfac4  remove-msgcat.patch"
