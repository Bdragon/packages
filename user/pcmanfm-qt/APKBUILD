# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=pcmanfm-qt
pkgver=0.13.0
pkgrel=0
pkgdesc="File manager and desktop icon manager for LXQt"
url="https://lxqt.org/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=0.5.0 liblxqt-dev
	libfm-qt-dev>=0.13.0 qt5-qtx11extras-dev qt5-qttools-dev
	kwindowsystem-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/pcmanfm-qt/releases/download/$pkgver/pcmanfm-qt-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DPULL_TRANSLATIONS=False \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="7b0d4ca5274e369e3af3c4b332a95ae78af54142aee39aae36832e85ca27d1f2f7509f00dc29d18a1e5a2558c8a82acd6569d2433acf13b2f95174c47779c089  pcmanfm-qt-0.13.0.tar.xz"
