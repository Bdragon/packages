# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ki18n
pkgver=5.48.0
pkgrel=0
pkgdesc="Framework for creating multi-lingual software"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1-only"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtscript-dev qt5-qtdeclarative-dev"
makedepends="$depends_dev cmake extra-cmake-modules doxygen graphviz
	qt5-qttools-dev"
install=""
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/ki18n-$pkgver.tar.xz
	libintl.patch"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="b757ce1d0dd1becbe658f628f4840cf82d5abf97ccad7657305166a5c7928b315269e9641773cb17d7bf02cba043d24f962578038a07f2de10cdffc186452af2  ki18n-5.48.0.tar.xz
b2b74bf07e49e0451b880fadea5d2a1566f69bb7f7751bef97f29a4cb35e08a144bcf5c2dc5f793cb71fdd4504237140af1ab98b4372bf1a89d534f5ec9aeedd  libintl.patch"
