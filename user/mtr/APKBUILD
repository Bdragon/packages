# Contributor: John Keith Hohm <john@hohm.net>
# Maintainer:
pkgname=mtr
pkgver=0.92
pkgrel=1
pkgdesc="Full screen ncurses traceroute tool"
url="https://www.bitwizard.nl/mtr/"
arch="all"
options="!check suid"  # Tests need networking.
license="GPL-2.0-only AND LGPL-3.0-only AND BSD-3-Clause"
depends="ncurses"
makedepends="gtk+2.0-dev libcap-dev ncurses-dev"
subpackages="$pkgname-doc $pkgname-gtk"
source="https://www.bitwizard.nl/$pkgname/files/$pkgname-$pkgver.tar.gz
	mtr-gtk.desktop
	"

build() {
	cd "$builddir"
	mkdir -p mtr curses gtk

	export GIT_DIR="$builddir"
	export LIBS="-ltinfo"
	cd "$builddir"/curses
	../configure --prefix=/usr \
		--without-gtk
	make

	cd "$builddir"/gtk
	../configure --prefix=/usr \
		--with-gtk \
		--program-suffix=-gtk
	make
	unset LIBS GIT_DIR
}

package() {
	cd "$builddir"/curses
	make DESTDIR="$pkgdir" install
	cd "$builddir"/gtk
	make DESTDIR="$pkgdir" install
	install -D -m 644 "$srcdir"/mtr-gtk.desktop \
		"$pkgdir"/usr/share/applications/mtr-gtk.desktop
	install -D -m 644 "$builddir"/img/mtr_icon.xpm \
		"$pkgdir"/usr/share/pixmaps/mtr_icon.xpm
}

gtk() {
	pkgdesc="Graphical traceroute tool"
	mkdir -p "$subpkgdir"/usr/sbin "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/sbin/mtr*-gtk "$subpkgdir"/usr/sbin/
	mv "$pkgdir"/usr/share/applications "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/pixmaps "$subpkgdir"/usr/share/
}

sha512sums="d362a418b6c17fd2d08da1ed9e033fa3330e4c0497e1bb0644f6193d23f4e20dd8ee181942c2a20ec0025a8b96c521394a21be5a1d9036f8a0a8c4cddfbe381f  mtr-0.92.tar.gz
ecf7543e0125fad6d3f17c30f29f1fc8a3b1e2e477802fe8464e436c3cdfa30d0630b8543cc3f022c475228e94ac8f92981df4d8fb08fe01d004be3d78d6da77  mtr-gtk.desktop"
