# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=krita
pkgver=4.1.1
pkgrel=1
pkgdesc="Digital painting program by KDE"
url="https://krita.org/"
arch="all"
options="!check"  # Tests require X11.
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev karchive-dev kconfig-dev
	kwidgetsaddons-dev kcompletion-dev kcoreaddons-dev kguiaddons-dev
	ki18n-dev kitemmodels-dev kitemviews-dev kwindowsystem-dev zlib-dev
	qt5-qtsvg-dev qt5-qtmultimedia-dev kcrash-dev libice-dev libx11-dev
	libxcb-dev fftw-dev libpng-dev boost-dev tiff-dev libjpeg-turbo-dev
	kio-dev eigen-dev exiv2-dev lcms2-dev poppler-qt5-dev gsl-dev libxi-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/krita/$pkgver/krita-$pkgver.tar.gz"

prepare() {
	cd "$builddir"
	default_prepare
	mkdir -p build
}

build() {
	cd "$builddir"/build
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DEIGEN3_INCLUDE_DIR=/usr/include/eigen3 \
		${CMAKE_CROSSOPTS} \
		..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="51b7527a3a7b049fc7d8dcd586ef35b574904b1bfa837975cfac055f182413d6c38d6fe81df4948bb4276f5e8a39dc5c981305ff0c42b4a1239b39adaa881904  krita-4.1.1.tar.gz"
