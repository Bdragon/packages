# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: 
pkgname=tmux
pkgver=2.7
pkgrel=0
pkgdesc="Tool to control multiple terminals from a single terminal"
url="https://tmux.github.io/"
arch="all"
license="MIT"
depends="ncurses-terminfo"
makedepends="bsd-compat-headers libevent-dev libutempter-dev ncurses-dev"
subpackages="$pkgname-doc"
source="https://github.com/tmux/tmux/releases/download/$pkgver/$pkgname-$pkgver.tar.gz"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install

	install -Dm644 example_tmux.conf \
		"$pkgdir"/usr/share/doc/$pkgname/examples/$pkgname.conf
	for file in CHANGES README TODO; do
		install -m644 "$file" "$pkgdir"/usr/share/doc/$pkgname/
	done
}

sha512sums="7839ef748ea55df8c02c727047f65bd235b5e3b8ab23157246071e1b9954fa269594da9fbd0fabf6a850e3b5dfda962a0a067c1507411c92a84d1db2666ecf37  tmux-2.7.tar.gz"
