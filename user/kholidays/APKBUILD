# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kholidays
pkgver=5.48.0
pkgrel=0
pkgdesc="List of national holidays for many countries"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1"
depends=""
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtbase-dev
	qt5-qtdeclarative-dev qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kholidays-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	# Requires *actual* *locale* *support*!
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E testholidayregion
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="70bf5b48e08d5a250205b30e62108944aa01a9df497e653765d3225166239395c9b88ec8a2622309e19b644f256727331723ffa96dc7a1ea55e36568f933536f  kholidays-5.48.0.tar.xz"
