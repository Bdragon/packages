# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=powerdevil
pkgver=5.12.6
pkgrel=0
pkgdesc="KDE Plasma power management utilities"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev
	kactivities-dev kauth-dev kconfig-dev kdbusaddons-dev kglobalaccel-dev
	ki18n-dev kidletime-dev kio-dev knotifyconfig-dev kdelibs4support-dev
	kwayland-dev libkscreen-dev plasma-workspace-dev solid-dev eudev-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/powerdevil-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="b77e1990e59bae684d19fd796789aa0216d111952941642c354fc760e862cd5008c923aa5f1783c8699818fdfd36333a2ad56832834e3ce2232905ec399e0846  powerdevil-5.12.6.tar.xz"
