# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=lximage-qt
pkgver=0.7.0
pkgrel=0
pkgdesc="Image viewer and screenshot tool for LXQt"
url="https://lxqt.org/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=0.5.0 libexif-dev
	libfm-qt-dev>=0.12.0 qt5-qtx11extras-dev qt5-qttools-dev qt5-qtsvg-dev
	kwindowsystem-dev"
source="https://github.com/lxqt/lximage-qt/releases/download/$pkgver/lximage-qt-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DPULL_TRANSLATIONS=False \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="a31185562dec1aa720287ab3f9fb79b9bccfcf0f2069aa404bd469852edbe2507f8c24cf8af0cbe2ef7013e4dab8ca51c00ac7a348254f1bf20458decd3c82b4  lximage-qt-0.7.0.tar.xz"
