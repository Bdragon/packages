# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libarchive
pkgver=3.3.2
pkgrel=4
pkgdesc="Library for creating and reading streaming archives"
url="http://libarchive.org/"
arch="all"
license="BSD-2-Clause AND BSD-3-Clause AND Public-Domain"
makedepends="zlib-dev bzip2-dev xz-dev lz4-dev acl-dev openssl-dev expat-dev
	attr-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools"
source="http://www.libarchive.org/downloads/$pkgname-$pkgver.tar.gz
	seek-error.patch
	CVE-2017-14166.patch"
options="!check"  # needs EUC-JP and KOI8R support in iconv
builddir="$srcdir/$pkgname-$pkgver"

# secfixes:
#   3.3.2-r1:
#     - CVE-2017-14166

build () {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--with-expat \
		--without-xml2 \
		--with-bz2lib \
		--with-zlib \
		--with-lzma \
		--with-lz4 \
		--enable-acl \
		--enable-xattr \
		ac_cv_header_linux_fiemap_h=no
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="libarchive tools - bsdtar and bsdcpio"

	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
	ln -s bsdtar "$subpkgdir"/usr/bin/tar
}

sha512sums="1e538cd7d492f54b11c16c56f12c1632ba14302a3737ec0db786272aec0c8020f1e27616a7654d57e26737e5ed9bfc9a62f1fdda61a95c39eb726aa7c2f673e4  libarchive-3.3.2.tar.gz
ff2567f243ba7e9ce20bc4f7fa422a922c5c23049004efdd8f71f29f93ab9be9aadd4c100e8c6dca318442d583fbad9bd6466017a23f83af18b9808c718b9fce  seek-error.patch
7cc9dbafd970c07fb4421b7a72a075cc0a000db77df4432222539c58625c93c45f01a144838b551980bc0c6dc5b4c3ab852eb1433006c3174581ba0897010dbe  CVE-2017-14166.patch"
