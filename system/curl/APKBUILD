# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: Elizabeth Myers <elizabeth@adelielinux.org>
pkgname=curl
pkgver=7.60.0
pkgrel=0
pkgdesc="An URL retrival utility and library"
url="http://curl.haxx.se"
arch="all"
license="MIT"
depends="ca-certificates"
makedepends_build="groff perl"
makedepends_host="zlib-dev openssl-dev libssh2-dev"
makedepends="$makedepends_build $makedepends_host"
source="http://curl.haxx.se/download/$pkgname-$pkgver.tar.bz2
	"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-dev libcurl"

# secfixes:
#   7.60.0-r0:
#     - CVE-2017-8816
#     - CVE-2017-8817
#     - CVE-2017-8818
#     - CVE-2018-1000005
#     - CVE-2018-1000007
#     - CVE-2018-1000120
#     - CVE-2018-1000121
#     - CVE-2018-1000122
#     - CVE-2018-1000300
#     - CVE-2018-1000301
#   7.56.1-r0:
#     - CVE-2017-1000257
#   7.55.0-r0:
#     - CVE-2017-1000099
#     - CVE-2017-1000100
#     - CVE-2017-1000101
#   7.54.0-r0:
#     - CVE-2017-7468
#   7.53.1-r2:
#     - CVE-2017-7407
#   7.53.0:
#     - CVE-2017-2629
#   7.52.1:
#     - CVE-2016-9594
#   7.51.0:
#     - CVE-2016-8615
#     - CVE-2016-8616
#     - CVE-2016-8617
#     - CVE-2016-8618
#     - CVE-2016-8619
#     - CVE-2016-8620
#     - CVE-2016-8621
#     - CVE-2016-8622
#     - CVE-2016-8623
#     - CVE-2016-8624
#     - CVE-2016-8625
#   7.50.3:
#     - CVE-2016-7167
#   7.50.2:
#     - CVE-2016-7141
#   7.50.1:
#     - CVE-2016-5419
#     - CVE-2016-5420
#     - CVE-2016-5421
#   7.36.0:
#     - CVE-2014-0138
#     - CVE-2014-0139

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-ipv6 \
		--enable-unix-sockets \
		--without-libidn \
		--without-libidn2 \
		--disable-ldap \
		--with-pic
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

libcurl() {
	pkgdesc="The multiprotocol file transfer library"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/lib "$subpkgdir"/usr
}

sha512sums="c7566bbe7289cd75d34a65b457905d54b5d07543b9fed5a762c889eb09114ad66de62c3edafd1973e87bc8e303a434e77b4e40eea1718801e79ae9256531abe9  curl-7.60.0.tar.bz2"
